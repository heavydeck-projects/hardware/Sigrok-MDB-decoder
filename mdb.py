## Copyright (C) 2021 J. Luis Álvarez <root@heavydeck.net>
##
## This program is free software; you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation; either version 2 of the License, or
## (at your option) any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with this program; if not, see <http://www.gnu.org/licenses/>.
##

from enum import Enum

class MdbStates(Enum):
    IDLE                = 0
    VMC_TRANSMIT        = 1
    PERIPHERAL_TRANSMIT = 2

#All constants taken from MDB spec revision 4.3
#Comments and comment strings may reference specificacion sections by using
#the following text: [SS: <section>]

MAX_BLOCK_SIZE = 36  #<- SS: 2.2
MDB_ACK = 0x00

#Helper functions for MDB
class Mdb():
        def getAddress(byte):
            'Extract address from MDB first byte on a block [SS: 2.2]'
            return (byte & 0xF8) >> 3

        def getPeripheralCommand(byte):
            'Extract the command number from the VMC byte [SS: 2.2]'
            return (byte & 0x07)

class MdbDecoder():
    def __init__(self, decoder):
        #Sigrok decoder subclass instance
        self.decoder = decoder

        #FSM initial state
        self.state   = MdbStates.IDLE

        #Data from the bus
        self.pdata  = None
        self.byte   = None
        self.mode   = None
        self.is_vmc = None

        #Buffers
        self.vmc_buffer        = None
        self.peripheral_buffer = None

        #Status
        self.vmc_ss = None #<- Start sample of a VMC block
        self.vmc_es = None #<- End sample of a VMC block

    def vmcTransmitStart(self):
        self.vmc_buffer = list()
        self.vmc_buffer.append(self.byte)
        self.vmc_ss = self.decoder.current_ss
        self.decoder.print_debug("VMC Start")
    
    def vmcTransmitContinue(self):
        self.vmc_buffer.append(self.byte)
        self.vmc_es = self.decoder.current_es
        self.decoder.print_debug("VMC continuation")
    
    def vmcTransmitFinish(self):
        pass

    def peripheralTransmitStart(self):
        self.peripheral_buffer = list()
        self.peripheral_buffer.append(self.byte)
        self.peripheral_ss = self.decoder.current_ss
        self.decoder.print_debug("Dev Start")

    def peripheralTransmitContinue(self):
        self.peripheral_buffer.append(self.byte)
        self.peripheral_es = self.decoder.current_es
        self.decoder.print_debug("Dev Continuation")
    
    def peripheralTransmitFinish(self, single_byte=False):
        if single_byte:
            self.peripheral_buffer = list()
        self.peripheral_buffer.append(self.byte)
        self.peripheral_es = self.decoder.current_es
        self.decoder.print_debug("Dev End")
    
    def peripheralAck(self):
        self.decoder.print_debug("Dev ACK")
    
    def vmcAck(self):
        self.decoder.print_debug("VMC ACK")

    def process_idle(self):
        pass

    #VMC state machine
    def process_byte(self, pdata, is_vmc):
        #Update decoder class attributes
        self.byte, self.bits = pdata
        self.mode = ((self.byte >> 8) & 0x01) == 1
        self.byte = self.byte & 0xFF
        self.is_vmc = is_vmc

        #IDLE -> VMC_TRANSMIT
        #When VMC sends a byte with the mode bit set
        if (self.state == MdbStates.IDLE) and (is_vmc) and (self.mode == True):
            self.vmcTransmitStart()
            self.state = MdbStates.VMC_TRANSMIT

        #VMC_TRANSMIT -> VMC_TRANSMIT
        #While receiving continuation bytes from VMC, keep adding them.
        elif (self.state == MdbStates.VMC_TRANSMIT) and (is_vmc) and (self.mode == False):
            self.vmcTransmitContinue()

        #VMC_TRANSMIT -> VMC_TRANSMIT
        #If we receive another byte from VMC with mode bit set start a new transmission
        #and raise a warning. This may happen if device doesn't answer (timeout)
        elif (self.state == MdbStates.VMC_TRANSMIT) and (is_vmc) and (self.mode == True):
            self.vmcTransmitStart()
            #ToDo Raise a warning

        #VMC_TRANSMIT -> PERIPHERAL_TRANSMIT
        #After receiving the first byte after the VMC finishes transmission, terminate the 
        #VMC block and start parsing the peripheral bytes
        elif (self.state == MdbStates.VMC_TRANSMIT) and (not is_vmc) and (self.mode == False):
            self.vmcTransmitFinish()
            self.peripheralTransmitStart()
            self.state = MdbStates.PERIPHERAL_TRANSMIT

        #VMC_TRANSMIT -> IDLE
        #If Peripheral immediatly responds with mode bit set, parse it and go back to IDLE
        elif (self.state == MdbStates.VMC_TRANSMIT) and (not is_vmc) and (self.mode == True):
            self.peripheralTransmitFinish(single_byte=True)
            self.state = MdbStates.IDLE

        #PERIPHERAL_TRANSMIT -> PERIPHERAL_TRANSMIT
        #While receiving bytes from peripheral with the mode byte not set, keep adding them
        elif (self.state == MdbStates.PERIPHERAL_TRANSMIT) and (not is_vmc) and (self.mode == False):
            self.peripheralTransmitContinue()
        
        #PERIPHERAL_TRANSMIT -> IDLE
        #When receiving a byte from the device with the mode bit set, finish the peripheral
        #response.
        elif (self.state == MdbStates.PERIPHERAL_TRANSMIT) and (not is_vmc) and (self.mode == True):
            self.peripheralTransmitFinish()
            self.state = MdbStates.IDLE

        #Otherwise... Print debug
        else:
            msg = "(State %s) 0x%02X" % (self.state, self.byte,)
            if self.mode:
                msg += "*"
            self.decoder.print_debug(msg)
