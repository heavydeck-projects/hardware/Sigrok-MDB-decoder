## Copyright (C) 2021 J. Luis Álvarez <root@heavydeck.net>
##
## Based on libsigrok MIDI decoder:
## Copyright (C) 2013-2016 Uwe Hermann <uwe@hermann-uwe.de>
## Copyright (C) 2016 Chris Dreher <chrisdreher@hotmail.com>
##
## This program is free software; you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation; either version 2 of the License, or
## (at your option) any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with this program; if not, see <http://www.gnu.org/licenses/>.
##

import sigrokdecode as srd
from .util import *
from .mdb import *

from enum import Enum

RX = 0
TX = 1

class Decoder(srd.Decoder):
    api_version = 3
    id = 'mdb'
    name = 'MDB/ICP'
    longname = 'Multidrop Bus/Internal Communication Protocol'
    desc = 'Vending machine peripheral protocol.'
    license = 'gplv2+'
    inputs = ['uart']
    outputs = []
    tags = ['Embedded/industrial']
    
    options = (
        {'id': 'sample_opt', 'desc': 'Sample Description', 'default': 'A', 'values': ('A', 'B')},
    )
    
    annotations = (
        ('vmc-command', 'VMC TX'),    #0
        ('dev-response', 'VMC RX'),   #1
        ('mdb-annotations', 'MDB'),   #2
        ('mdb-debug', 'Debug Info'),  #3
    )
    annotation_rows = (
        ('commands', 'Commands', (0, 1)),
        ('bus', 'MDB', (2,)),
        ('debug', 'Debug info', (3,)),
    )

    def __init__(self):
        self.reset()
        
    def metadata(self, key, value):
        #Save samplerate
        if key == srd.SRD_CONF_SAMPLERATE:
            self.samplerate = value

    def reset(self):
        # --- Attributes ---
        self.current_ss = 0
        self.current_es = 0

        self.mdb = MdbDecoder(self)

    def start(self):
        self.out_ann = self.register(srd.OUTPUT_ANN)
        
    def die(self, msg=""):
        raise Exception("Death at: %fms -> %fms" % (sampleToTime(self.samplerate, self.current_ss)*1000,sampleToTime(self.samplerate, self.current_es)*1000,) )
        
    def print_debug(self, msg):
        self.put(self.current_ss, self.current_es, 0, [3, [msg]])

    def decode(self, ss, es, data):
        ptype, rxtx, pdata = data

        #Save current start/end samples
        self.current_ss = ss
        self.current_es = es
        
        #pdata comes as a value byte and idividual bits/samples as a list
        #like the fllowing
        # [
        #  byte_value:int,
        #  [
        #    [ bit_value:int, start_sample:int, end_sample:int ]
        #    (...)
        #  ]
        # ]

        if ptype == 'IDLE':
            self.mdb.process_idle()
            return
        elif ptype != 'DATA':
            #Everything but data gets ignored
            return

        self.mdb.process_byte(pdata, rxtx == TX)
